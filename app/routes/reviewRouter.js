//khai báo thư viện express
const express = require("express");

//khai báo middleware
const { 
    reviewMiddleware,
    getAllReviewMiddleware,
    getReviewMiddleware,
    createReviewMiddleware,
    updateReviewMiddleware,
    deleteReviewMiddleware
} = require("../middlewares/reviewMiddleware");

//tạo review router
const reviewRouter = express.Router();

//khai báo middleware
reviewRouter.use(reviewMiddleware)

//Load all review
reviewRouter.get('/reviews', getAllReviewMiddleware, (req, res) => {
    res.status(200).json({
        message:"Get All review"
    })
})

//Load a review
reviewRouter.get('/reviews/:reviewId', getReviewMiddleware, (req, res) => {
    var reviewId = req.params.reviewId
    res.status(200).json({
        message:"Get review id = " + reviewId
    })
})

//Create new review
reviewRouter.post('/reviews', createReviewMiddleware, (req, res) => {
    res.status(200).json({
        message:"Create new review"
    })
})

//Update a review
reviewRouter.put('/reviews/:reviewId', updateReviewMiddleware, (req, res) => {
    var reviewId = req.params.reviewId
    res.status(200).json({
        message:"Update review id = " + reviewId
    })
})

//Delete a review
reviewRouter.delete('/reviews/:reviewId', deleteReviewMiddleware, (req, res) => {
    var reviewId = req.params.reviewId
    res.status(200).json({
        message:"Delete review id = " + reviewId
    })
})

module.exports = { reviewRouter }