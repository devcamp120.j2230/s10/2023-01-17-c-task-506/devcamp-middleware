//khai báo thư viện express
const express = require("express");

//khai báo middleware
const { 
    courseMiddleware, 
    getAllCourseMiddleware,
    getCourseMiddleware,
    createCourseMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
} = require("../middlewares/courseMiddleware");

//tạo course router
const courseRouter = express.Router();

//khai báo middleware
courseRouter.use(courseMiddleware)

//Load all course
courseRouter.get('/courses', getAllCourseMiddleware, (req, res) => {
    res.status(200).json({
        message:"Get All Course"
    })
})

//Load a course
courseRouter.get('/courses/:courseId', getCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId
    res.status(200).json({
        message:"Get Course id = " + courseId
    })
})

//Create new course
courseRouter.post('/courses', createCourseMiddleware, (req, res) => {
    res.status(200).json({
        message:"Create new Course"
    })
})

//Update a course
courseRouter.put('/courses/:courseId', updateCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId
    res.status(200).json({
        message:"Update Course id = " + courseId
    })
})

//Delete a course
courseRouter.delete('/courses/:courseId', deleteCourseMiddleware, (req, res) => {
    var courseId = req.params.courseId
    res.status(200).json({
        message:"Delete Course id = " + courseId
    })
})

module.exports = { courseRouter }